from sanic import Sanic, response
from sanic.exceptions import NotFound
from sanic.response import json
from blueprints.api import api

app = Sanic()


# Static files configuration
app.static('/', './static')  # Access to all static files by url
app.static('', './static/index.min.html')  # Redirect for base url to index.html
app.static('/cv', './static/res/cv.pdf')  # Custom link for CV
app.static('/404', './static/404.min.html')  # Custom link to 404


# Blueprints
app.blueprint(api)


# 404 Error Handler
@app.exception(NotFound)
def exception_404(request, exception):
    return response.redirect('/404')


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, workers=4)
