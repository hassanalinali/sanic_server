from sanic.response import json
from sanic import Blueprint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from validate_email import validate_email

api = Blueprint('user', url_prefix='api/v1/user')

Base = declarative_base()

from models.user import User

engine = create_engine('sqlite:///database/sqlalchemy.db')
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)


@api.route('/create', methods=['POST'])
async def user_create(request):
    session = DBSession()

    userEmail = request.form.get('email')
    userName = request.form.get('name')
    userPass = request.form.get('password')

    if (userEmail is None or userName is None or userPass is None):
        return json({'success': False, 'error_code': 1, 'error_message': 'Invalid user creditentials'})

    if (not validate_email(userEmail)):  # verify=True for DNS verification, too slow though
        return json({'success': False, 'error_code': 2, 'error_message': 'Invalid user email'})

    user = User(email=userEmail, name=userName, password=userPass)
    session.add(user)
    session.commit()
    return json({'success': True})


@api.route('/get', methods=['POST'])
async def user_get_by_id(request):
    session = DBSession()

    userEmail = request.form.get('email')
    userId = request.form.get('id')

    if (userId is not None):
        currentUser = session.query(User).filter(User.id == id).first()
        if (currentUser is None):
            return json({'success': False, 'error_code': 3, 'error_message': 'No user'})
        else:
            return json({'success': True, 'email': currentUser.email})

    elif (userEmail is not None):
        currentUser = session.query(User).filter(User.email == userEmail).first()
        if (currentUser is None):
            return json({'success': False, 'error_code': 3, 'error_message': 'No user'})
        else:
            return json({'success': True, 'email': currentUser.email})

    else:
        return json({'success': False, 'error_code': 1, 'error_message': 'Invalid user creditentials'})


@api.route('/delete', methods=['POST'])
async def user_delete(request):
    session = DBSession()

    userId = request.form.get('id')
    userEmail = request.form.get('email')

    if (userEmail is None and userId is None):
        return json({'success': False, 'error_code': 1, 'error_message': 'Invalid user creditentials'})

    if (userId is not None):
        currentUser = session.query(User).fliter(User.id == userId).first()
        if (currentUser is None):
            return json({'success': False, 'error_code': 3, 'error_message': 'No user'})
        else:
            return json({'success': True, 'email': currentUser.email})

    else:
        currentUser = session.query(User).fliter(User.email == userEmail).first()
        if (currentUser is None):
            return json({'success': False, 'error_code': 3, 'error_message': 'No user'})
        else:
            return json({'success': True, 'email': currentUser.email})
